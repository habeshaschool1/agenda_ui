from urllib.parse import urlparse

from flask import flash

from agenda_ui.helper.constants import ALERT_ERROR, ALERT_SUCCESS


def change_filter_to_dic(filter_str=""):
    """ this method will get a string with query filter format of key=value&&key=value
    and return a json key value form"""

    if "?" in filter_str:
        filter_str = filter_str.split("?")[1]

    filter_json = {}
    if "&" in filter_str:
        pairs = filter_str.split("&")
        for pair in pairs:
            if "=" in pair:
                key, value = pair.split("=")
                filter_json[key] = value
    return filter_json


def get_url_info(url_data, key):
    url_data = urlparse(url_data)

    if key == 'url':
        hostname = url_data.hostname
        return hostname
    elif key == 'port':
        port = url_data.port or (443 if url_data.scheme == 'https' else 80)
        return port
    elif key == 'query':
        query = url_data.query
        return query
    elif key == 'netloc':
        netloc = url_data.netloc
        return netloc
    elif key == 'path':
        path = url_data.path
        return path
    else:
        return "no key"


def flash_success(msg_txt, category=ALERT_SUCCESS):
    """ this method will help to have customized flash messages"""
    msg_txt = msg_txt.lower()
    if "segment" in msg_txt or "token has expired" in msg_txt:
        msg_txt ="login required"
        # TODO signout user here
    flash(msg_txt, category)

def flash_alert(msg_txt, category=ALERT_ERROR):
    """ this method will help to have customized flash messages"""
    if "segment" in msg_txt or "token has expired" in msg_txt:
        msg_txt = "login required"
        # TODO signout user here
    flash(msg_txt, category)
