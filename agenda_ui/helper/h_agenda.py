import json
import unicodedata

import requests

from agenda_ui.helper.constants import api_base_uri


def api_get_agendas(agenda_id=None, filter={}):
    uri = f"{api_base_uri}/agendas/{agenda_id}"
    resp = requests.get(uri)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    return body
