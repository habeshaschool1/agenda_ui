import json
import unicodedata

import requests
from flask import request

from agenda_ui.helper.constants import api_base_uri


def api_get_users(bearer, filter={}):
    headers = {
        'Content-Type': "application/json",
        'Authorization': f"Bearer {bearer}"
    }

    uri = f"{api_base_uri}/users/"
    resp = requests.get(uri, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    return body


def api_get_users_who_created_agendas():
    uri = f"{api_base_uri}/agendas/users/"
    resp = requests.get(uri)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    return body


def api_get_dashboard_report():
    uri = f"{api_base_uri}/dashboard/"
    resp = requests.get(uri)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    return body


def api_get_user_report(bearer):
    uri = f"{api_base_uri}/reports/users/"
    headers = {
        'Content-Type': "application/json",
        'Authorization': f"Bearer {bearer}"
    }
    resp = requests.get(uri, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    print(body)
    return body

def api_get_current_user(bearer):
    uri = f"{api_base_uri}/current_user"
    headers = {
        'Content-Type': "application/json",
        'Authorization': f"Bearer {bearer}"
    }
    resp = requests.get(uri, headers=headers)
    #if resp.status_code == 422:
    #    body =
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    return body


def api_get_users_public_info():
    bearer = request.cookies.get('Bearer')
    headers = {
        'Content-Type': "application/json",
        'Authorization': f"Bearer {bearer}"
    }

    uri = f"{api_base_uri}/users/public_info"
    resp = requests.get(uri, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    return body


def api_get_users_public_info_dict():
    bearer = request.cookies.get('Bearer')
    headers = {
        'Content-Type': "application/json",
        'Authorization': f"Bearer {bearer}"
    }

    uri = f"{api_base_uri}/users/public_info"
    resp = requests.get(uri, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    users = {}
    for user in body['result']:
        users[str(user["user_id"])] = user
    return users
