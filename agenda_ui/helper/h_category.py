import json
import unicodedata

import requests

from agenda_ui.helper.constants import api_base_uri


def api_get_categories(filter={}):
    uri = f"{api_base_uri}/categories/"
    resp = requests.get(uri)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    return body


def api_get_states_list(filter={}):
    uri = f"{api_base_uri}/agendas/states-list"
    resp = requests.get(uri)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    return body


def api_get_states_map(filter={}):
    uri = f"{api_base_uri}/agendas/states-map/{filter}"
    resp = requests.get(uri)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    return body
