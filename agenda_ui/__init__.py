"""Initialize app."""
from flask import Flask
from flask_consent import Consent


def create_app():
    """Construct the core agenda_ui."""
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object('config.Config')
    app.config['RECAPTCHA_PUBLIC_KEY'] = 'iubhiukfgjbkhfvgkdfm'
    app.config['RECAPTCHA_PARAMETERS'] = {'size': '100%'}
    app.config['SECRET_KEY'] = 'any secret string'
    app.config['CONSENT_FULL_TEMPLATE'] = 'consent.html'
    app.config['CONSENT_BANNER_TEMPLATE'] = 'consent_banner.html'
    consent = Consent(app)
    consent.add_standard_categories()

    with app.app_context():
        # Import parts of our agenda_ui
        from . import routes

        return app
