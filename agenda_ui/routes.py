import datetime
import json
import unicodedata

import babel as babel
import requests
from babel import Locale
from flask import current_app as app
from flask import url_for, render_template, redirect, request, flash, make_response

from .forms import AddAgendaForm, SignupForm, AddCategoryForm, SigninForm, PutAgendaForm, ForumAgendaForm, \
    PutAgendaRules, UpdateProfile, UpdateEmail, UpdatePassword
from .helper.common import get_url_info, flash_alert, flash_success
from .helper.constants import api_base_uri, DASHBOARD_REPORT
from .helper.h_agenda import api_get_agendas
from .helper.h_category import api_get_categories, api_get_states_map
from .helper.h_user import api_get_users, api_get_users_who_created_agendas, api_get_user_report, \
    api_get_dashboard_report, api_get_current_user


# route to the main page
@app.route('/')
def home():
    bearer = request.cookies.get('Bearer')
    current_user = ""
    user_report = DASHBOARD_REPORT

    if bearer != None:
        # get logged in user if any
        current_user = api_get_current_user(bearer)
        # get report of logged in user if any
        user_report = api_get_user_report(bearer)
        if 'verification failed' in user_report.get('msg', 'verification failed'):
            user_report = DASHBOARD_REPORT

    # get generic report about all agendas that anyone can see it
    dashboard_report = api_get_dashboard_report()

    return render_template('index.jinja2',
                           template='home-template', current_user=current_user,
                           user_report=user_report, dashboard_report=dashboard_report)


# route to "Add agenda" page
@app.route('/addagenda', methods=['GET', 'POST'])
def addagenda():
    form = AddAgendaForm(request.form)
    uri = f"{api_base_uri}/agendas/"

    if request.method == 'POST' and form.validate():
        bearer = request.cookies.get('Bearer')
        headers = {
            'Content-Type': "application/json",
            'Authorization': f"Bearer {bearer}"
        }

        title = form.title.data
        catid = int(form.catid.data)
        desc = form.desc.data
        body = {"title": title, "category_id": catid, "desc": desc}
        resp = requests.post(uri, json=body, headers=headers)
        body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
        body = json.loads(body)
        msg_txt = body.get("msg")

        if resp.status_code <= 300:
            agenda_id = body["result"]["agenda_id"]
            agenda_title = body["result"]["title"]
            flash_success(f"{msg_txt}. Agenda ID is {agenda_id} with title {agenda_title}")
            return redirect(url_for('agendas'))
        else:
            flash_alert(msg_txt)

    uriC = f"{api_base_uri}/categories/"
    resp = requests.get(uriC)
    categories = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    categories = dict(json.loads(categories))
    categories = categories["result"]
    category_list = [("", "---")]
    for category in categories:
        category_list.append((category["category_id"], category["title"]))

    form.catid.choices = category_list

    return render_template('addagenda.jinja2',
                           form=form,
                           template='form-template')


# route to "List of agendas" page
@app.route('/agendas')
def agendas():
    path = get_url_info(request.url, 'path')
    query = get_url_info(request.url, 'query')
    if query != "":
        filter = f"{path}?{query}"
    else:
        filter = path

    uri = f"{api_base_uri}{filter}"

    resp = requests.get(uri)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)

    bearer = request.cookies.get('Bearer')
    headers = {
        'Content-Type': "application/json",
        'Authorization': f"Bearer {bearer}"
    }

    report = {}

    if bearer != None:
        uri = f"{api_base_uri}/reports/users"
        resp = requests.get(uri, headers=headers)
        report = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
        report = json.loads(report)

    # get list of categories
    categories_body = api_get_categories()

    # get list of states
    states = {
        "result": {
            "edit": "Edit",
            "review": "Review",
            "cancel": "Cancel",
            "rate": "Rate",
            "reject": "Reject",
            "forum": "Forum",
            "vote": "Vote",
            "close": "Close"
        }
    }

    # get list of owners
    owners = api_get_users_who_created_agendas()

    resp = make_response(
        render_template('agendas.jinja2', info=body, report=report, categories=categories_body, states=states,
                        owners=owners))

    if "?" in filter:
        filter = filter.split("?")[1]
    else:
        filter = ""

    resp.set_cookie('agenda_filter', filter)

    return resp
    return render_template('agendas.jinja2', info=body, report=report, categories=categories_body, states=states,
                           owners=owners)


# route to forward get with agenda_id to "List of agendas" page
@app.route('/agendas/<agenda_id>')
def agendas_with_id(agenda_id):
    return redirect(url_for('agendas'))


# route to "Agenda detail" page with update agenda, update agenda state, update agenda rules
@app.route('/putagenda/<agenda_id>', methods=['GET', 'PUT', 'POST'])
def updateagenda(agenda_id):
    form = PutAgendaForm(request.form)
    form_rules = PutAgendaRules(request.form)

    uri = f"{api_base_uri}/agendas/{agenda_id}"

    if request.method == 'POST' and form.title.data != "":
        print("post where form title is not empty")
        bearer = request.cookies.get('Bearer')
        headers = {
            'Content-Type': "application/json",
            'Authorization': f"Bearer {bearer}"
        }

        title = form.title.data
        catid = int(form.catid.data)
        desc = form.desc.data
        body = {"title": title, "category_id": catid, "desc": desc}
        resp = requests.put(uri, json=body, headers=headers)
        body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
        body = json.loads(body)
        msg_txt = body.get("msg")

        if resp.status_code <= 300:
            agenda_id = body["result"]["agenda_id"]
            agenda_title = body["result"]["title"]
            flash_success(f"{msg_txt}. Agenda ID is {agenda_id} with title {agenda_title}")
        else:
            flash_alert(msg_txt)

    if request.method == 'POST' and form_rules.rate_min_num_participants.data is not None:
        print("post from form rules")
        bearer = request.cookies.get('Bearer')
        headers = {
            'Content-Type': "application/json",
            'Authorization': f"Bearer {bearer}"
        }

        rate_min_num_participants = int(form_rules.rate_min_num_participants.data)
        rate_min_like_persent = int(form_rules.rate_min_like_persent.data)
        forum_min_num_participants = int(form_rules.forum_min_num_participants.data)
        body = {
            "rate_min_num_participants": rate_min_num_participants,
            "rate_min_like_persent": rate_min_like_persent,
            "forum_min_num_participants": forum_min_num_participants
        }

        body = {
            "rules": {
                "forum": {
                    "min_number_of_participants": forum_min_num_participants
                },
                "rate": {
                    "min_number_of_participants": rate_min_num_participants,
                    "rate_like_percent": rate_min_like_persent
                }
            }
        }

        resp = requests.put(uri, json=body, headers=headers)
        body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
        body = json.loads(body)
        msg_txt = body.get("msg")

        if resp.status_code <= 300:
            agenda_id = body["result"]["agenda_id"]
            agenda_title = body["result"]["title"]
            flash_success(f"{msg_txt}. Agenda ID is {agenda_id} with title {agenda_title}")
        else:
            flash_alert(msg_txt)

    resp = requests.get(uri)
    agenda = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    agenda = dict(json.loads(agenda))
    agenda = agenda["result"]

    uriU = f"{api_base_uri}/current_user"
    bearer = request.cookies.get('Bearer')
    if bearer != None:
        headers = {
            'Content-Type': "application/json",
            'Authorization': f"Bearer {bearer}"
        }
        resp = requests.get(uriU, headers=headers)
        current_user = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
        current_user = json.loads(current_user)
    else:
        current_user = ""

    report = {}

    if bearer != None:
        uri = f"{api_base_uri}/reports/users"
        resp = requests.get(uri, headers=headers)
        report = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
        report = json.loads(report)

    uriC = f"{api_base_uri}/categories/"
    resp = requests.get(uriC)
    categories = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    categories = dict(json.loads(categories))
    categories = categories["result"]
    category_list = []
    for category in categories:
        category_list.append((category["category_id"], category["title"]))

    # get list of categories
    categories_body = api_get_categories()

    # get list of states
    current_state = agenda["state"]
    states = api_get_states_map(current_state)

    # filling agenda data for update agenda modal
    form.title.data = agenda["title"]
    form.desc.data = agenda["desc"]
    form.catid.choices = category_list

    # filling agenda rules data for update agenda rules modal
    form_rules.rate_min_num_participants.data = agenda["rules"]["rate"]["min_number_of_participants"]
    form_rules.rate_min_like_persent.data = agenda["rules"]["rate"]["rate_like_percent"]
    form_rules.forum_min_num_participants.data = agenda["rules"]["forum"]["min_number_of_participants"]

    return render_template('agenda_detail.jinja2',
                           form=form, form_rules=form_rules,
                           template='form-template', catid=agenda["category_id"],
                           agenda_id=agenda["agenda_id"], agenda=agenda,
                           current_user=current_user, categories=categories_body,
                           states=states, report=report)


# TODO might be back to explore search with collapse
@app.route('/agendas_search')
def agendas_search():
    return render_template('agendas_search.html')


# TODO might be back to explore nav bar with drop-down
@app.route('/nav_bar')
def nav_bar():
    return render_template('nav_bar.html')


# route to "Add category" page
@app.route('/addcategory', methods=['GET', 'POST'])
def addcategory():
    form = AddCategoryForm(request.form)
    uri = f"{api_base_uri}/categories/"
    if request.method == 'POST' and form.validate():
        bearer = request.cookies.get('Bearer')
        headers = {
            'Content-Type': "application/json",
            'Authorization': f"Bearer {bearer}"
        }
        title = form.title.data
        desc = form.desc.data
        body = {"title": title, "desc": desc}

        resp = requests.post(uri, json=body, headers=headers)
        body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
        body = json.loads(body)
        msg_txt = body.get("msg")


        if resp.status_code <= 300:
            category_id = body["result"]["category_id"]
            category_title = body["result"]["title"]
            flash_success(f"{msg_txt}. Category ID is {category_id} with title {category_title}")
            return redirect(url_for('categories'))
        else:
            flash_alert(msg_txt)

    return render_template('addcategory.jinja2',
                           form=form,
                           template='form-template')


# route to "List of categories" page
@app.route('/categories')
def categories():
    body = api_get_categories()
    return render_template('categories.jinja2', info=body)


# route to "Signup" page
@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignupForm()
    uri = f"{api_base_uri}/authentication/signup/"

    if request.method == 'POST' and form.validate():
        email = form.email.data
        password = form.password.data
        confirmPassword = form.confirmPassword.data
        country_code = form.country_code.data
        phone = form.phone.data
        name = form.name.data
        body = {"email": email, "password": password, "country_code": country_code, "phone": phone, "name": name}
        print(body)
        resp = requests.post(uri, json=body, verify=False)

        try:
            body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
            body = json.loads(body)
            msg_txt = body.get("msg", "duplicate data, please try with different data")
            if resp.status_code <= 300:
                user_id = body["result"]["user_id"]
                flash_success(f"{msg_txt}. Your user id is {user_id}")
                return redirect(url_for('signin'))
            else:
                flash_alert(msg_txt)

        except requests.exceptions.ConnectionError as r:
            flash_alert(body.get("msg", "Connection refused"))
            r.status_code = "Connection refused"

    return render_template('signup.jinja2',
                           form=form,
                           template='form-template')

# route to "Signin" page
@app.route('/signout', methods=['GET', 'POST'])
def signout():
    resp = make_response(redirect(url_for('home')))
    resp.set_cookie('Bearer', '', expires=0)
    return resp

# route to "Signin" page
@app.route('/signin', methods=['GET', 'POST'])
def signin():
    form = SigninForm()
    uri = f"{api_base_uri}/authentication/signin/"

    if request.method == 'POST' and form.validate():
        email = form.email.data
        password = form.password.data
        body = {"email": email, "password": password}
        resp = requests.post(uri, json=body)
        body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
        body = json.loads(body)

        if resp.status_code <= 300:
            msg_txt = body.get("msg", "Login success. Welcome to Agenda Application!")
            flash_success(msg_txt)
            #resp = make_response(render_template('success.jinja2'))
            resp = make_response(redirect(url_for('home')))
            resp.set_cookie('Bearer', body['result']['access_token'])
            return resp
            #return redirect(url_for('home'))
        else:
            msg_txt = body.get('msg', "something wrong to login")
            flash_alert(msg_txt)


    return render_template('signin.jinja2',
                           form=form,
                           template='form-template')


# route to "success" page
@app.route('/success', methods=['GET', 'POST'])
def success():
    return render_template('success.jinja2',
                           template='success-template')


# route to cancel agenda
@app.route('/delete/<agenda_id>', methods=['GET', 'PUT', 'POST'])
def delete(agenda_id):
    uri = f"{api_base_uri}/agendas/{agenda_id}"

    bearer = request.cookies.get('Bearer')
    headers = {
        'Content-Type': "application/json",
        'Authorization': f"Bearer {bearer}"
    }

    body = {"state": "cancel"}
    resp = requests.put(uri, json=body, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    msg_txt = body.get("msg")

    if resp.status_code <= 300:
        agenda_id = body["result"]["agenda_id"]
        agenda_title = body["result"]["title"]
        flash_success(f"{msg_txt}. You cancel Agenda ID is {agenda_id} with title {agenda_title}")
        # return redirect(url_for('success'))
    else:
        flash_alert(msg_txt)

    return body


# route to change state of agenda
@app.route('/change-state/<agenda_id>/<to_state>', methods=['GET', 'PUT', 'POST'])
def change_state(agenda_id, to_state):
    uri = f"{api_base_uri}/agendas/change-state/{agenda_id}"

    bearer = request.cookies.get('Bearer')
    headers = {
        'Content-Type': "application/json",
        'Authorization': f"Bearer {bearer}"
    }

    body = {"state": to_state}
    resp = requests.put(uri, json=body, headers=headers)
    resp_change_state = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    resp_change_state = json.loads(resp_change_state)
    msg_txt = resp_change_state.get("msg")

    if resp.status_code <= 300:
        agenda_id = resp_change_state["result"]["agenda_id"]
        agenda_title = resp_change_state["result"]["title"]
        flash_success(f"{msg_txt}. You changed state for Agenda ID is {agenda_id} with title {agenda_title}")
        return resp_change_state
        #return redirect(url_for('updateagenda'))
    else:
        flash_alert(msg_txt)
        return resp_change_state
    #return render_template('agenda_detail.jinja2', resp_change_state=resp_change_state)


# NOTHING most probably there is nothing behind this route
@app.route('/rates/<int:agenda_id>/<action>/<method>', methods=['POST', 'GET'])
def rates(agenda_id, action, method):
    uri = f"{api_base_uri}/{agenda_id}"
    if method == 'POST':
        bearer = request.cookies.get('Bearer')
        headers = {
            'Content-Type': "application/json",
            'Authorization': f"Bearer {bearer}"
        }
        rate_choice = action
        if action == 'like':
            body = {"like": "true"}
        if action == 'dislike':
            body = {"like": "false"}

        resp = requests.post(uri, json=body, headers=headers)
        body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
        body = json.loads(body)

        return render_template('success.jinja2',
                               template='success-template')


# route to handle 404 error
@app.errorhandler(404)
def page_not_found(error):
    return render_template('page404.jinja2'), 404

# route to handle 500 error
@app.errorhandler(500)
def server_error(error):
    return render_template('page500.jinja2'), 500

# route for footer with copyright
@app.context_processor
def my_utility_processor():
    def date_now(format="%d.m.%Y %H:%M:%S"):
        """ returns the formatted datetime """
        return datetime.datetime.now().strftime(format)

    def name():
        """ returns our company name """
        return "Habesha School Ltd."

    return dict(date_now=date_now, company=name)


# route for rating agenda
@app.route("/<agenda_id>/clicked/<like>", methods=['GET', 'POST'])
def clickedRate(agenda_id, like):
    port = get_url_info(request.url, 'port')
    uri = f"{api_base_uri}/rates/{agenda_id}"

    bearer = request.cookies.get('Bearer')
    headers = {
        'Content-Type': "application/json",
        'Authorization': f"Bearer {bearer}"
    }

    body = {"like": True}
    if like == 'false':
        body = {"like": False}

    if bearer == None:
        msg_txt = "You are not authorized to rate agenda. Please proceed with sign in"
        flash_alert(msg_txt)
        return body

    resp = requests.post(uri, json=body, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)

    if resp.status_code <= 300:
        agenda_id = body["result"]["agenda_id"]
        msg_txt = body.get("msg")
        flash_success(f"You rated agenda with ID {agenda_id} and reply from server {msg_txt}")
        return body
    else:
        msg_txt = "post in troubles"
        flash_alert(msg_txt)

    uri = f"{api_base_uri}{request.url.split(port)[1]}"
    resp = requests.get(uri)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    return body


# route for voting agenda
@app.route("/<agenda_id>/voted/<vote>", methods=['GET', 'POST'])
def clickedVote(agenda_id, vote):
    port = get_url_info(request.url, 'port')
    uri = f"{api_base_uri}/votes/{agenda_id}"

    bearer = request.cookies.get('Bearer')
    headers = {
        'Content-Type': "application/json",
        'Authorization': f"Bearer {bearer}"
    }

    body = {"vote": vote}

    if bearer != None:
        resp = requests.post(uri, json=body, headers=headers)
        body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
        body = json.loads(body)

        if resp.status_code <= 300:
            pin = body["result"]["pin_code"]
            vote = body["result"]["vote"]
            agenda_id = body["result"]["agenda_id"]
            msg_txt = body.get("msg")
            flash_success(
                f"PIN {pin}: save your pin code for your vote {vote.upper()} reference on agenda {agenda_id}. {msg_txt}. There is no way to recover your PIN.")
            return body
        else:
            msg_txt = body.get("msg", "Post error")
            flash_alert(msg_txt)

    else:
        msg_txt = f"You are not authorized to vote agenda. Please proceed with sign in"
        flash_alert(msg_txt)
        return body

    uri = f"{api_base_uri}{request.url.split(port)[0]}"
    resp = requests.get(uri)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    return body
    return render_template('agendas.jinja2', info=body)


# TODO route for user statictics
@app.route('/user_report')
def user_report():
    bearer = request.cookies.get('Bearer')
    headers = {
        'Content-Type': "application/json",
        'Authorization': f"Bearer {bearer}"
    }

    uri = f"{api_base_uri}/reports/users"
    resp = requests.get(uri, headers=headers)
    report = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    report = json.loads(report)
    print(report)

    return render_template('user_report.jinja2', report=report)


# route for user profiles
@app.route('/user_profiles')
def user_profiles():
    bearer = request.cookies.get('Bearer')

    if bearer != None:
        body = api_get_users(bearer=bearer)
        return render_template('user_profiles.jinja2', info=body)
    else:
        msg_txt = f"You are not authorized to see user profile. Please proceed with sign in"
        flash_alert(msg_txt)
        return render_template('index.jinja2',
                               template='home-template')


# route for current user profile
@app.route('/user_profile', methods=['GET', 'POST', 'PUT'])
def user_profile():
    form_info = UpdateProfile(request.form)
    form_email = UpdateEmail(request.form)
    form_passwd = UpdatePassword(request.form)


    uri = f"{api_base_uri}/current_user"
    bearer = request.cookies.get('Bearer')
    if bearer != None:
        headers = {
            'Content-Type': "application/json",
            'Authorization': f"Bearer {bearer}"
        }
        resp = requests.get(uri, headers=headers)
        current_user = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
        current_user = json.loads(current_user)
    else:
        current_user = ""


    if request.method == 'POST':
        bearer = request.cookies.get('Bearer')
        headers = {
            'Content-Type': "application/json",
            'Authorization': f"Bearer {bearer}"
        }

        body = {}

        if form_info.name.data != '' or form_info.phone.data != '':
            print("CHANGE INFO 1")
            if form_info.name.data != None:
                name = str(form_info.name.data)
                phone = str(form_info.phone.data)

                body = {
                    "name": name,
                    "phone": phone
                }

        if  form_email.email.data != '':
            print("CHANGE EMAIL")
            if form_email.email.data is not None:
                email = str(form_email.email.data)
                body = {
                    "email": email
                }

        print(body)

        uriI = f"{api_base_uri}/users/{current_user['user_id']}"
        resp = requests.put(uriI, json=body, headers=headers)
        body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
        body = json.loads(body)
        msg_txt = body.get("msg")

        if resp.status_code <= 300:
            flash_success(msg_txt)
        else:
            flash_alert(msg_txt)


    if request.method == 'POST'  and form_passwd.validate():
        if form_passwd.password.data != "":
            print("CHANGE PASSWORD")
            bearer = request.cookies.get('Bearer')
            headers = {
                'Content-Type': "application/json",
                'Authorization': f"Bearer {bearer}"
            }

            passwdOld = str(form_passwd.passwordOld.data)
            passwd = str(form_passwd.password.data)
            print(passwd)
            body = {
                "old_password": passwdOld,
                "new_password": passwd
            }
            uriP = f"{api_base_uri}/authentication/change_password/{current_user['user_id']}"
            print(uriP)
            resp = requests.put(uriP, json=body, headers=headers)
            print(resp.text)
            body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
            body = json.loads(body)
            print(body)
            msg_txt = body.get("msg")

            if resp.status_code <= 300:
                flash_success(msg_txt)
            else:
                flash_alert(msg_txt)



    bearer = request.cookies.get('Bearer')
    current_user = ""

    if bearer != None:
        current_user = api_get_current_user(bearer)

    # filling info data for update info modal
    if current_user == "":
        form_info.name.data = 0
        form_info.phone.data = 0
    else:
        form_info.name.data = current_user["name"]
        form_info.phone.data = current_user["phone"]


    return render_template('user_profile.jinja2', user_info=current_user,
                       form_info=form_info, form_email=form_email, form_passwd=form_passwd,
                       template='form-template')



# route for forum_agenda with agenda_id
@app.route('/forum_agenda/<agenda_id>', methods=['GET', 'POST'])
def forum_agenda(agenda_id):
    form = ForumAgendaForm(request.form)

    bearer = request.cookies.get('Bearer')
    headers = {
        'Content-Type': "application/json",
        'Authorization': f"Bearer {bearer}"
    }

    uri = f"{api_base_uri}/forums/{agenda_id}"

    if request.method == 'POST' and form.validate():
        comment = form.comment.data
        body = {"comment": comment}

        resp = requests.post(uri, json=body, headers=headers)
        body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
        body = json.loads(body)
        msg_txt = body.get("msg")

        if resp.status_code <= 300:
            pass
            # flash_success(f"{msg_txt}. You commented agenda")
            # return redirect(url_for('/forum_agenda'))
        else:
            flash_alert(msg_txt)

    resp = requests.get(uri, headers=headers)
    report = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    report = json.loads(report)

    agenda_info = api_get_agendas(agenda_id)

    # form.comment.data = ""

    return render_template('forum_agenda.jinja2', form=form,
                           template='form-template', report=report, agenda=agenda_info, all=False)


# route for forum_agenda
@app.route('/forum_agendas')
def forum_agendas():
    bearer = request.cookies.get('Bearer')
    headers = {
        'Content-Type': "application/json",
        'Authorization': f"Bearer {bearer}"
    }

    uri = f"{api_base_uri}/forums"
    resp = requests.get(uri, headers=headers)
    report = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    report = json.loads(report)

    agenda_info = api_get_agendas()

    return render_template('forum_agenda.jinja2', report=report, agenda=agenda_info, all=True)


# TODO route to update agendas report
@app.route('/update_agendas_reports', methods=['GET', 'PUT', 'POST'])
def update_agendas_reports():
    uri = f"{api_base_uri}/reports"

    bearer = request.cookies.get('Bearer')
    headers = {
        'Content-Type': "application/json",
        'Authorization': f"Bearer {bearer}"
    }

    resp = requests.put(uri, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    msg_txt = body.get("msg")

    if resp.status_code <= 300:
        msg_txt = body.get("msg")
        flash_success(msg_txt)
        return redirect(url_for('success'))
    else:
        flash_alert(msg_txt)


# TODO route to update categories report
@app.route('/update_categories_reports', methods=['GET', 'PUT', 'POST'])
def update_categories_reports():
    uri = f"{api_base_uri}/reports/categories"

    bearer = request.cookies.get('Bearer')
    headers = {
        'Content-Type': "application/json",
        'Authorization': f"Bearer {bearer}"
    }

    resp = requests.put(uri, headers=headers)
    body = unicodedata.normalize('NFKD', resp.text).encode('ascii', 'ignore')
    body = json.loads(body)
    msg_txt = body.get("msg")

    if resp.status_code <= 300:
        msg_txt = body.get("msg")
        flash_success(msg_txt)
        return redirect(url_for('success'))
    else:
        flash_alert(msg_txt)


# route for date converting
@app.template_filter()
def format_datetime(value, date_format='medium'):
    babel.dates.LC_TIME = Locale.parse('en_US')
    value = value['$date']
    value = datetime.datetime.fromtimestamp(value / 1000.0)

    if date_format == 'full':
        # date_format = "EEEE, MMMM dd, y HH:mm:ss"
        date_format = "EEEE, dd-MMM-yyyy hh:mm:ss a"
    elif date_format == 'medium':
        # date_format = "EEEE, MMMM dd, y HH:mm"
        date_format = "EE, dd-MMM-yyyy hh:mm a"
    elif date_format == 'small':
        date_format = "MMMM dd, y"

    return babel.dates.format_datetime(value, date_format, locale='en_US')


# route for date converting
@app.template_filter()
def paragraph_resizer(text, size_to_split=600, less_spad_id='dots', more_span_id='more', id_js_name='readMore'):

    if len(text) > size_to_split:
        before = text[:size_to_split]
        after = text[size_to_split:]
        text = f"<p>{before}<span id='{less_spad_id}'>...</span><span id='{more_span_id}'>{after}</span></p><button onclick='{id_js_name}()' id='{id_js_name}'>Read more</button>"
    else:
        text = f"<p>{text}</p>"


    return text


# route to get RGBA coloring for users on forum page
@app.template_filter()
def get_rgba(num):
    num_str = str(num)
    length = len(num_str)
    last_digit = int(num_str[-1])
    middle_digit = int(num_str[length // 2])
    first_digit = int(num_str[0])
    is_last_digit_even = last_digit % 2 == 0
    is_first_digit_even = first_digit % 2 == 0
    is_middle_digit_even = middle_digit % 2 == 0

    print(num)
    print(first_digit)
    print(last_digit)
    print(is_last_digit_even)
    print(is_first_digit_even)

    if is_last_digit_even and is_middle_digit_even and is_first_digit_even:
        color_num = (middle_digit * 10 + last_digit)
        print((200, 100, color_num))
        return (200 + first_digit * 10, 100 + middle_digit * 10, color_num)
    elif is_last_digit_even and is_middle_digit_even and not is_first_digit_even:
        color_num = (100 + middle_digit * 10 + last_digit)
        print((155, color_num, 255))
        return (100 + + first_digit * 10, 200 + middle_digit * 10, color_num)
    elif is_last_digit_even and is_middle_digit_even and is_first_digit_even:
        color_num = (100 + middle_digit * 10 + last_digit)
        print((255, color_num, 155))
        return (75 + first_digit * 10, 200 + middle_digit * 10, color_num)
    elif is_last_digit_even and is_middle_digit_even and is_first_digit_even:
        color_num = (100 + middle_digit * 10 + last_digit)
        print((50 + first_digit * 10, 50 + middle_digit * 10, color_num))
        return (155, color_num, 255)
    elif not is_last_digit_even and is_middle_digit_even and is_first_digit_even:
        color_num = (100 + middle_digit * 10 + last_digit)
        return (25 + first_digit * 10, 25 + middle_digit * 10, color_num)
    elif is_last_digit_even and is_middle_digit_even and is_first_digit_even:
        color_num = (100 + middle_digit * 10 + last_digit)
        print((10 + first_digit * 10, 10 + middle_digit * 10, color_num))
        return (155, color_num, 255)

    else:
        color_num = (200 + last_digit)
        print((color_num, 155, 255))
        return (first_digit * 10, 30 + middle_digit * 10, color_num)


@app.context_processor
def inject_template_scope():
    injections = dict()

    def cookies_check():
        value = request.cookies.get('cookie_consent')
        return value == 'true'
    injections.update(cookies_check=cookies_check)

    return injections